from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeClassifier
from nltk.corpus import stopwords
from collections import Counter
from sklearn.svm import SVC
from sklearn import metrics
from spacy import displacy
import en_core_web_sm
import pandas as pd
import unidecode
import spacy
import re

data = pd.read_csv('train.csv', encoding='utf-8-sig', delimiter=",")
dataTest = pd.read_csv('test.csv', encoding='utf-8-sig', delimiter=",")

features = data['sentence']
labels = data['label']

featuresTest = dataTest['sentence']
labelsTest = dataTest['label']

# make a stence in lowercase
def lowerCase(sentence):
    return sentence.lower()

# Convert Accented Characters
def removeAccented(sentence):
    return unidecode.unidecode(sentence)

# remove stopwords 
def stopWords(sentence):
    newSentence = ""
    stop_words = set(stopwords.words('french'))
    for word in sentence.split(" "):
        if word not in stop_words:
            newSentence += " "+word
    return newSentence          

# remove the noise
def removeNoise(sentence):
    sentence = re.sub("\%", "", sentence)
    sentence = re.sub("\/", "", sentence)
    sentence = re.sub("s+", "", sentence)
    return sentence

# Named Entity Recognition
def EntityRecognition(sentence):
    nlp = en_core_web_sm.load()
    text = nlp(sentence)
    print([(X.text, X.label_) for X in text.ents])

def Pre_processing(sentence):
    sentence = lowerCase(sentence)
    sentence = removeAccented(sentence)
    sentence = stopWords(sentence)
    sentence = removeNoise(sentence)
    #EntityRecognition(sentence)
    return sentence

def get_top_n_words(corpus, n=None):
    vec = CountVectorizer().fit(corpus)
    bag_of_words = vec.transform(corpus)
    sum_words = bag_of_words.sum(axis=0) 
    words_freq = [(word, sum_words[0, idx]) for word, idx in vec.vocabulary_.items()]
    words_freq =sorted(words_freq, key = lambda x: x[1], reverse=True)
    return words_freq[:n]

def Visualisation(data):
    common_words = get_top_n_words(data, 20)
    for word, freq in common_words:
        print(word, freq)
    df1 = pd.DataFrame(common_words, columns = ['ReviewText' , 'count'])
    df1.groupby('ReviewText').sum()['count'].sort_values(ascending=False).iplot(
        kind='bar', yTitle='Count', linecolor='black', title='Top 20 words in review before removing stop words')

def Y(y):
    targets = []
    for val in y:
        if "," in str(y):
            val = str(val).replace(",",".")
        targets.append(float(val))
    return targets


def splitData(sentences, labels):
    X_train, X_test, y_train, y_test = train_test_split(sentences, labels, test_size=0.05, random_state=100)
    return [X_train, X_test, y_train, y_test]
    
def Train(features,featuresTest, labels, labelsTest):
    
    [x_train, x_test, y_train, y_test] = splitData(features, labels)
    
    classifier = LogisticRegression()
    classifier.fit(x_train, y_train)
    score = classifier.score(x_train, y_train)
    print("score train LogisticRegression :", score)
    score = classifier.score(x_test, y_test)
    print("score validation LogisticRegression :",score)
    
    pred = classifier.predict(featuresTest)
    accuracy = metrics.accuracy_score(labelsTest, pred)
    print("accuracy LogisticRegression :",accuracy)
    
    classifier = KNeighborsClassifier(n_neighbors=1)
    classifier.fit(x_train,y_train)
    print("score train KNN :",classifier.score(x_train,y_train))
    score = classifier.score(x_test, y_test)
    print("score validation KNN :",score)
    pred = classifier.predict(featuresTest)
    accuracy = metrics.accuracy_score(labelsTest, pred)
    print("accuracy KNN :",accuracy)
    
    classifier = DecisionTreeClassifier(criterion = "gini",random_state = 100,max_depth=100, min_samples_leaf=1)
    classifier.fit(x_train,y_train)
    print("score train Decision Tree :",classifier.score(x_train,y_train))
    score = classifier.score(x_test, y_test)
    print("score validation Decision Tree :",score)
    pred = classifier.predict(featuresTest)
    accuracy = metrics.accuracy_score(labelsTest, pred)
    print("accuracy Decision Tree :",accuracy)
    
    RandomForestClassifier(random_state = 1, n_estimators = 10, min_samples_split = 1)
    classifier.fit(x_train,y_train)
    print("score train Random Forest :",classifier.score(x_train,y_train))
    score = classifier.score(x_test, y_test)
    print("score validation Random Forest :",score)
    pred = classifier.predict(featuresTest)
    accuracy = metrics.accuracy_score(labelsTest, pred)
    print("accuracy Random Forest :",accuracy)
    
    classifier = SVC(kernel='linear')
    classifier.fit(x_train,y_train)
    print("score train SVM :",classifier.score(x_train,y_train))
    score = classifier.score(x_test, y_test)
    print("score validation SVM :",score)
    pred = classifier.predict(featuresTest)
    accuracy = metrics.accuracy_score(labelsTest, pred)
    print("accuracy SVM :",accuracy)
    
def Train2(features, featuresTest, targets, targetsTest):
    [x_train, x_test, y_train, y_test] = splitData(features, targets)
    classifier = LinearRegression()
    classifier.fit(x_train,y_train)
    print('score train regression Linear :',classifier.score(x_train,y_train))
    score = classifier.score(x_test, y_test)
    print("score validation regression Linear :",score)
    # pred = classifier.predict(featuresTest)
    # accuracy = metrics.accuracy_score(targetsTest, pred)
    # print("accuracy regression Linear :",accuracy)
    
def main():
    global features
    global featuresTest
    
    sentences = []
    sentencesTest = []
    
    for sentence in features:
        sentences.append(Pre_processing(sentence))
    
    Visualisation(sentences)
    
    for sentence in featuresTest:
        sentencesTest.append(Pre_processing(sentence))
    print("______________________________________Bag of Words___________________________")
    vectorizer = CountVectorizer()
    vectorizer.fit(sentences)
    features = vectorizer.transform(sentences)
    featuresTest = vectorizer.transform(sentencesTest)
    Train(features,featuresTest, labels, labelsTest)
    
    # print("----------------reg-------------")
    # targets = Y(labels)
    # targetsTest = Y(labelsTest)
    # Train2(features, featuresTest, targets, labelsTest)
    
    print("______________________________________TfIdf___________________________")
    vectorizer = TfidfVectorizer()
    vectorizer.fit(sentences)
    features = vectorizer.transform(sentences)
    featuresTest = vectorizer.transform(sentencesTest)
    Train(features, featuresTest, labels, labelsTest)
    
if __name__=='__main__':
    main()
    
    
    
    
    
    
    
        
        
    
    