import json
  
f = open('OCR/10604.json',)
  
data = json.load(f)


listY = []
dictionnary = {}

for i in range(len(data['pageFiles']['1']['vertices'])):
    gravityCenter = data['pageFiles']['1']['vertices'][i]['gravityCenter']
    gravityCenterX = gravityCenter['x']
    gravityCenterY = gravityCenter['y']
    listY.append(gravityCenterY)
    
    dictionnary[data['pageFiles']['1']['vertices'][i]['text']] = {gravityCenterY, gravityCenterX}

ySorted = sorted(listY)

dictionnary2 = {}

for ySort in ySorted:
    listX = []
    for i in range(len(data['pageFiles']['1']['vertices'])):
        gravityCenter = data['pageFiles']['1']['vertices'][i]['gravityCenter']
        gravityCenterX = gravityCenter['x']
        gravityCenterY = gravityCenter['y']
        
        if ySort == gravityCenterY:
            listX.append(gravityCenterX)
    listX = sorted(listX)  
    dictionnary2[ySort] = listX

listYSet = set(ySorted)
index = 0
dicte = {}
for ySet in listYSet:
    dicte[ySet] = index
    index += 1
    
plot_data = [[]] * len(listYSet) 

for y in dictionnary2.keys():
    for x in dictionnary2[y]:
        for k, v in dictionnary.items():
            if {y, x} == v:
                plot_data[dicte[y]].append(k)

for liste in plot_data:
    line = " ".join(liste)
    print("hhh \n" ,line)

f.close()